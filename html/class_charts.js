var class_charts =
[
    [ "Charts", "class_charts.html#a0ef4c6221f08230f220249819db5553a", null ],
    [ "~Charts", "class_charts.html#ac9bbfce71a5bd499492c187ab4c01803", null ],
    [ "addPointLight", "class_charts.html#a5b634a5806f25a6ee7426c4924f3f801", null ],
    [ "addPointTemperature", "class_charts.html#a3ca84bb48c691a64ab934d98065fccea", null ],
    [ "clearDateLight", "class_charts.html#a8b0a56ba22323286e89c5ab11da4ef3f", null ],
    [ "clearDateTemp", "class_charts.html#a3b12f9f0afb1483e6baf270ac4fe5ecf", null ],
    [ "on_pushButtonClear_clicked", "class_charts.html#a7d6a5c4ffd993ec8244b2251447a8669", null ],
    [ "plot", "class_charts.html#a80936268e79e369aba7cb3da9707e54a", null ],
    [ "plotLight", "class_charts.html#a515e8578e6136c0b9b6b988f206c161f", null ],
    [ "plotTemperature", "class_charts.html#ac574c6e106bc899e2e237c9b992f1a9a", null ],
    [ "receiveDataLight", "class_charts.html#a2a1f9d5f5e2467ca110b9d16f3f519ac", null ],
    [ "receiveDataTemperature", "class_charts.html#a16b6867b6c3562618ac34f87c10430a0", null ],
    [ "chart", "class_charts.html#ac965a22db3f2b6d20b13a2d2fcc01f36", null ],
    [ "qv_x", "class_charts.html#a23e924a64f8eb24f72e2bd504c1c5db1", null ],
    [ "qv_y", "class_charts.html#a860ca525ea39c90a4c3be74d44b1ed5f", null ],
    [ "qvtemp_x", "class_charts.html#a81372673833c04110f5f0a6dac13d62e", null ],
    [ "qvtemp_y", "class_charts.html#aced42741a891449352a939df15c855e0", null ],
    [ "series", "class_charts.html#aa7aac5f56bc8873475c0066018317d40", null ],
    [ "ui", "class_charts.html#a268e5ce72b256234652eff68f5bdd15d", null ]
];