var searchData=
[
  ['seccounter_0',['secCounter',['../class_main_window.html#a13b093b467d1c61588ff264e38798090',1,'MainWindow']]],
  ['sendlight_1',['sendLight',['../class_main_window.html#a84ab18eaf9d0538fab0633c14e904136',1,'MainWindow']]],
  ['sendtemperature_2',['sendTemperature',['../class_main_window.html#ab10ee656bb6432339396941ce7366e72',1,'MainWindow']]],
  ['series_3',['series',['../class_charts.html#aa7aac5f56bc8873475c0066018317d40',1,'Charts']]],
  ['setupdistanceandlightlabels_4',['setupDistanceAndLightLabels',['../class_main_window.html#ad9b674d6c8fb116f7243bd3b63348865',1,'MainWindow']]],
  ['setupdistanceandrobotlabel_5',['setupDistanceAndRobotLabel',['../class_main_window.html#a0dc6304889ce707b854f2d07f078cb8c',1,'MainWindow']]],
  ['setuplightmap_6',['setupLightMap',['../class_main_window.html#a7d58db4aaa37b07a39f01edeb4459b74',1,'MainWindow']]],
  ['setuplighttable_7',['setupLightTable',['../class_main_window.html#ac5ec00193d8d9ee873ea6680a5374042',1,'MainWindow']]],
  ['setuprobot_8',['setupRobot',['../class_main_window.html#a2004cf00ef788c96e7d14fdf715d5510',1,'MainWindow']]],
  ['setupstatuslabel_9',['setupStatusLabel',['../class_main_window.html#a354de8b79b7996530d6689c1a68cf858',1,'MainWindow']]],
  ['setuptemperaturemap_10',['setupTemperatureMap',['../class_main_window.html#afd0734e5f7f4c4feff97887fa7139720',1,'MainWindow']]],
  ['setuptemperaturetable_11',['setupTemperatureTable',['../class_main_window.html#a50d703bbf5276ff2e4e67a76caf455dc',1,'MainWindow']]],
  ['sizelightbd_12',['SizeLightBD',['../class_main_window.html#a91810c1d649437c7e3af1e7d64e63a77',1,'MainWindow']]],
  ['sizerobot_13',['SizeRobot',['../class_main_window.html#afceffd16aa8cecf375f612d79f2ad975',1,'MainWindow']]]
];
