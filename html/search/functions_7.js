var searchData=
[
  ['sendlight_0',['sendLight',['../class_main_window.html#a84ab18eaf9d0538fab0633c14e904136',1,'MainWindow']]],
  ['sendtemperature_1',['sendTemperature',['../class_main_window.html#ab10ee656bb6432339396941ce7366e72',1,'MainWindow']]],
  ['setupdistanceandlightlabels_2',['setupDistanceAndLightLabels',['../class_main_window.html#ad9b674d6c8fb116f7243bd3b63348865',1,'MainWindow']]],
  ['setupdistanceandrobotlabel_3',['setupDistanceAndRobotLabel',['../class_main_window.html#a0dc6304889ce707b854f2d07f078cb8c',1,'MainWindow']]],
  ['setuplightmap_4',['setupLightMap',['../class_main_window.html#a7d58db4aaa37b07a39f01edeb4459b74',1,'MainWindow']]],
  ['setuplighttable_5',['setupLightTable',['../class_main_window.html#ac5ec00193d8d9ee873ea6680a5374042',1,'MainWindow']]],
  ['setuprobot_6',['setupRobot',['../class_main_window.html#a2004cf00ef788c96e7d14fdf715d5510',1,'MainWindow']]],
  ['setupstatuslabel_7',['setupStatusLabel',['../class_main_window.html#a354de8b79b7996530d6689c1a68cf858',1,'MainWindow']]],
  ['setuptemperaturemap_8',['setupTemperatureMap',['../class_main_window.html#afd0734e5f7f4c4feff97887fa7139720',1,'MainWindow']]],
  ['setuptemperaturetable_9',['setupTemperatureTable',['../class_main_window.html#a50d703bbf5276ff2e4e67a76caf455dc',1,'MainWindow']]]
];
