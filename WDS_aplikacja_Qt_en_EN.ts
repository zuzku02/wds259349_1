<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Charts</name>
    <message>
        <location filename="charts.ui" line="14"/>
        <source>Dialog</source>
        <translation>Diagrams</translation>
    </message>
    <message>
        <location filename="charts.ui" line="24"/>
        <source>Temperatura</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="charts.ui" line="34"/>
        <source>Natężenie światła</source>
        <translation>Light intensity</translation>
    </message>
    <message>
        <location filename="charts.ui" line="66"/>
        <source>Wyczyść</source>
        <translation>Clear</translation>
    </message>
    <message>
        <location filename="charts.ui" line="73"/>
        <source>Zamknij</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Main Window</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="37"/>
        <location filename="mainwindow.ui" line="450"/>
        <source>Temperatura</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <source>Położenie</source>
        <translation type="vanished">Position</translation>
    </message>
    <message>
        <source>X</source>
        <translation type="vanished">X</translation>
    </message>
    <message>
        <source>Y</source>
        <translation type="vanished">Y</translation>
    </message>
    <message>
        <source>Dodaj</source>
        <translation type="vanished">Add</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="95"/>
        <location filename="mainwindow.ui" line="328"/>
        <source>Natężenie światła</source>
        <translation>Light intensity</translation>
    </message>
    <message>
        <source>Zatrzymaj (R)</source>
        <translation type="vanished">Stop (R)</translation>
    </message>
    <message>
        <source>Przód (T)</source>
        <translation type="vanished">Forward (T)</translation>
    </message>
    <message>
        <source>Przód (W)</source>
        <translation type="vanished">Forward (W)</translation>
    </message>
    <message>
        <source>Przód (O)</source>
        <translation type="vanished">Forward (O)</translation>
    </message>
    <message>
        <source>Tył (K)</source>
        <translation type="vanished">Reverse (K)</translation>
    </message>
    <message>
        <source>Tył (G)</source>
        <translation type="vanished">Reverse (G)</translation>
    </message>
    <message>
        <source>Tył (A)</source>
        <translation type="vanished">Reverse (A)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="50"/>
        <location filename="mainwindow.ui" line="117"/>
        <source>Godzina:</source>
        <translation>Time:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="55"/>
        <source>Temperatura:</source>
        <translation>Temperature:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="125"/>
        <source>Natężenie światła: </source>
        <translation>Light intensity: </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <location filename="mainwindow.ui" line="592"/>
        <source>Odległość</source>
        <translation>Distance</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="307"/>
        <source>Parametry</source>
        <translation>Parameters</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="369"/>
        <location filename="mainwindow.ui" line="489"/>
        <source>Aktualna wartość:</source>
        <translation>Current value:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="380"/>
        <location filename="mainwindow.ui" line="513"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <location filename="mainwindow.ui" line="462"/>
        <location filename="mainwindow.ui" line="627"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="431"/>
        <location filename="mainwindow.ui" line="526"/>
        <location filename="mainwindow.ui" line="603"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="576"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="743"/>
        <source>Połącz</source>
        <translation>Connect</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="710"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="770"/>
        <source>Wyczyść</source>
        <translation>Clear</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="721"/>
        <source>Odczytaj</source>
        <translation>Read</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="732"/>
        <source>Wykresy</source>
        <translation>Diagrams</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <source>Zamknij</source>
        <translation>Close</translation>
    </message>
    <message>
        <source>Godzina</source>
        <translation type="vanished">Time</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="mainwindow.cpp" line="39"/>
        <source>Okno główne</source>
        <translation>Main Window</translation>
    </message>
    <message>
        <source>Godzina</source>
        <translation type="vanished">Time</translation>
    </message>
    <message>
        <source>Temperatura</source>
        <translation type="vanished">Temperature</translation>
    </message>
    <message>
        <source>Natężenie światła</source>
        <translation type="vanished">Light intensity</translation>
    </message>
    <message>
        <source>Odległość</source>
        <translation type="vanished">Distance</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="569"/>
        <source>Szukam urządzeń...</source>
        <translation>Searching for devices...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="578"/>
        <source>Nazwa portu: </source>
        <translation>Port name: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="580"/>
        <source>Nie znaleziono urządzenia STMicroelectronics STLink Virtual COM Port.</source>
        <translation>No STMicroelectronics STLink Virtual COM Port device found.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="591"/>
        <source>Otwarto port szeregowy.</source>
        <translation>Serial port opened.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="598"/>
        <source>Otwarcie porty szeregowego się nie powiodło!</source>
        <translation>Failed to open the serial port!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="599"/>
        <source>Błąd</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="599"/>
        <source>Nie udało się połączyć z urządzeniem.</source>
        <translation>Failed to connect to the device.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="604"/>
        <source>Port już jest otwarty!</source>
        <translation>Port is already open!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="620"/>
        <source>Utracono połączenie z urządzeniem!</source>
        <translation>Connection to the device lost!</translation>
    </message>
    <message>
        <source>kąt: </source>
        <translation type="vanished">Angle: </translation>
    </message>
    <message>
        <location filename="charts.cpp" line="15"/>
        <source>Wykresy</source>
        <translation>Diagrams</translation>
    </message>
    <message>
        <location filename="charts.cpp" line="20"/>
        <location filename="charts.cpp" line="27"/>
        <source>Czas[s]</source>
        <translation>Time [s]</translation>
    </message>
    <message>
        <location filename="charts.cpp" line="21"/>
        <source>Temperatura [℃]</source>
        <translation>Temperature [℃]</translation>
    </message>
    <message>
        <location filename="charts.cpp" line="28"/>
        <source>Natężenie światła [lx]</source>
        <translation>Light intensity [lx]</translation>
    </message>
</context>
</TS>
