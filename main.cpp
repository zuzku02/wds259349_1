
#include "mainwindow.h"

#include <QApplication>

#include <QLocale>
#include <QTranslator>
#include <QInputDialog>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
   /* QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "WDS_aplikacja_Qt_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }*/
    MainWindow w;
 //   QObject::connect(&w, &MainWindow::windowResized, &w, &MainWindow::setupLightMap);
    w.change_language(0);

    w.show();
    return a.exec();
}
