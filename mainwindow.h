/*!
 * \file mainwindow.h
 * \brief Plik nagłówkowy klasy MainWindow
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QSerialPortInfo>
#include <QDateTime>
#include <QSerialPort>
#include <QRegExp>
#include <QTimer>
#include <QtCharts>
#include <QChartView>
#include <QPoint>
#include <QLineSeries>
#include <QDialog>
#include <QTableWidget>
#include <QMessageBox>
#include "charts.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


/*!
 * \class MainWindow
 * \brief Główne okno programu.
 * Klasa reprezentuje główne okno programu. Odpowiada za odbieranie danych z urządzenia, 
 * wyświetlanie danych pomiarowych w tabelach, wysyłania sygnałów do okna Charts oraz interakcję z użytkownikiem.
 */

class MainWindow : public QMainWindow

{
    Q_OBJECT

signals:
/*!
* \brief Sygnał wysyłający dane pomiarowe temperatury
* \param[in] x - czas wyrażony w sekundach, w którym odczytana została wartość temperatury 
* \param[in] temperature - wartość temperatury odczytanej z urządzenia w stopniach Celcjusza
*/
    void sendTemperature(double x, double temperature);
 /*!
 * \brief Sygnał wysyłający dane pomiarowe natężenia światła
 * \param[in] x - czas wyrażony w sekundach, w którym odczytana została wartość natężenia światła 
 * \param[in] light - wartość natężenia światła odczytana z urządzenia w luksach
 */
    void sendLight(double x, double light);

    void robotPositionChanged(int robotX, int robotY);

    void windowResized();

protected:
    void resizeEvent(QResizeEvent *event) override
    {
        // Wywołanie metody resizeEvent z klasy bazowej
        QMainWindow::resizeEvent(event);

        // Wyemitowanie sygnału windowResized po zmianie rozmiaru okna
      //  emit windowResized();
    //    setupDistanceAndRobotLabel();
        setupDistanceAndLightLabels();
    }

public:
/*!
* \brief Konstruktor klasy MainWindow
* \param[in] parent - wskaźnik na obiekt klasy QWidget reprezentujący okno rodzica
*/
    MainWindow(QWidget *parent = nullptr);
/*!
 * \brief Destruktor klasy MainWindow
 */
    ~MainWindow();

public slots:
     void change_language(int index);
     void setupLightMap();

private slots:
/*!
* \brief Slot odpowiedzialny za czyszczenie danych z tabeli
*/
    void on_pushButtonClear_clicked();
/*!
 * \brief Slot odpowiedzialny za dodanie danych do tabeli temperatury
 */
    void addToTableTemp(double temperature);
 /*!
 * \brief Slot odpowiedzialny za dodanie danych do tabeli natężenia światła
 */
    void addToTableLight(double light);

/*!
 * \brief Slot odpowiedzialny za odczytanie danych z portu szeregowego
 */
    void readFromPort();
/*!
 * \brief Slot odpowiedzialny za dodanie nowych danych pomiarowych do tabel
 */
    void on_pushButtonAdd_clicked();
/*!
 * \brief Slot odpowiedzialny za wyświetlenie okna Charts
 */

    void on_pushButtonChart_clicked();
/*!
 * \brief Slot odpowiedzialny za nawiązanie połączenia z urządzeniem
 */
    void on_pushButtonConnection_clicked();

/*!
 * \brief Slot odpowiedzialny za obsługuje błędów związanych połączeniem z portem szeregowym
 * \param[in] error - Typ błędu portu szeregowego
 */

    void handleError(QSerialPort::SerialPortError error);
/*!
 * \brief Slot odpowiedzialny za dodawanie współrzędnych położenia robota, w celu wizualizacji temperatury na mapie
 */


    void changeColorTemperature(double temperature);
 /*!
 * \brief Slot odpowiedzialny za zmianę koloru poszczególnego pola na mapie natężenia światła
 */
    void changeColorLight(double light);
    void updateLightRobotLabel(double light);
    void updateDistanceLabel(double distance_I, double distance_II, double distance_III);


    void setupStatusLabel();
    void setupDistanceAndRobotLabel();
    void setupTemperatureTable();
    void setupLightTable();
  //  void setupDistanceTable();
    void setupTemperatureMap();

    void setupRobot();
    void setupDistanceAndLightLabels();

    void setupLegendTemperature();
    void setupLegendLight();

    float calculateAngle();

    //void resizeEvent(QResizeEvent *event);



private:
    Ui::MainWindow *ui;
    Charts *chart;
    QSerialPort *device;
    QTranslator t;
    int secCounter = 0;
    int readSlower=1;
    int clickCounter =0;
    double x =0.0;
    int SizeLightBD;
    int robotX;
    int robotY;
    bool isDataRead;

    int SizeRobot;
    QVector<QLabel*> temperatureLabels;
    QVector<QLabel*> lightLabels;
    QLabel* robotLabel;
    QLabel *robotLabelTemp;
    QLabel *robotDistanceLabel;
    QLabel* label_I;
    QLabel* label_II;
    QLabel* label_III;
    QLabel* light;
    QLabel* light_intesity;

    enum ColumnTemp
    {
        TIMETEMP, TEMPERATURE
    };
    enum ColumnLight
    {
        TIMELIGHT,INTENSITY
    };
    enum ColumnDistance
    {
        TIMEDISTANCE, DISTANCE
    };


    int lewo_przod=0;
    int prawo_przod=0;
    int maxSpeed=100;
    int minSpeed=-100;
    float current_angle=0.0;
    float angle=0.0;
    double distance_I=12;
    double distance_II=10;
    double distance_III=30;
    double light_intensity=1000;
    double temperature=21;
    QTimer *timer;
};

#endif // MAINWINDOW_H
