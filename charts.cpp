/*!
 * \file charts.cpp
 * \brief Implementacja klasy Charts, która odpowiada za wyświetlanie wykresów temperatury i natężenia światła.
 */

#include "charts.h"
#include "ui_charts.h"
#include "qcustomplot.h"

Charts::Charts(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Charts)
{
    ui->setupUi(this);
    setWindowTitle(QObject::tr("Wykresy"));

    ui->plotTemperature->addGraph();
    ui->plotTemperature->graph(0)->setScatterStyle(QCPScatterStyle::ssCircle);
    ui->plotTemperature->graph(0)->setLineStyle(QCPGraph::lsNone);
    ui->plotTemperature->xAxis->setLabel(QObject::tr("Czas[s]"));
    ui->plotTemperature->yAxis->setLabel(QObject::tr("Temperatura [℃]"));
    connect(parent, SIGNAL(sendTemperature(double,double)), this, SLOT(receiveDataTemperature(double,double)));

    ui->plotLight->addGraph();
    ui->plotLight->graph(0)->setScatterStyle(QCPScatterStyle::ssCircle);
    ui->plotLight->graph(0)->setLineStyle(QCPGraph::lsNone);
    ui->plotLight->xAxis->setLabel(QObject::tr("Czas[s]"));
    ui->plotLight->yAxis->setLabel(QObject::tr("Natężenie światła [lx]"));
    connect(parent, SIGNAL(sendLight(double,double)), this, SLOT(receiveDataLight(double,double)));

}


Charts::~Charts()
{
    delete ui;
}


/*!
 * Metoda dodająca punkt na wykres temperatury.
 * \param x wartość osi X odpowiadająca czasowi w sekundach
 * \param y wartość osi Y odpowiadająca wartości temperatury
 */

void Charts::addPointTemperature(double x, double y)
{
    qvtemp_x.append(x);
    qvtemp_y.append(y);
}

/*!
 * \brief Metoda dodająca punkt na wykres natężenia światła.
 * \param x wartość osi X odpowiadająca czasowi w sekundach
 * \param y wartość osi Y odpowiadająca wartości natężenia światła
 */


void Charts::addPointLight(double x, double y)
{
    qv_x.append(x);
    qv_y.append(y);
}

/*!
 * \brief Metoda czyszcząca dane na wykresie temperatury.
 */

void Charts::clearDateTemp()
{
      qvtemp_x.clear();
      qvtemp_y.clear();
}

/*!
 * \brief Metoda czyszcząca dane na wykresie natężenia światła.
 */

void Charts::clearDateLight()
{
      qv_x.clear();
      qv_y.clear();
}

/*!
 * \brief Metoda wyświetlająca wykres temperatury.
 */

void Charts::plotTemperature()
{
      ui->plotTemperature->graph(0)->setData(qvtemp_x, qvtemp_y);
      double y_max = *std::max_element(qvtemp_y.constBegin(), qvtemp_y.constEnd());
      ui->plotTemperature->yAxis->setRange(0, y_max * 1.2); // os Y będzie miała wartość większą niż wartość punktu
      ui->plotTemperature->graph(0)->rescaleAxes(true);
      ui->plotTemperature->replot();
      ui->plotTemperature->update();
}

/*!
 * \brief Metoda wyświetlająca wykres natężenia światła.
 */

void Charts::plotLight()
{
      ui->plotLight->graph(0)->setData(qv_x, qv_y);
      double y_max = *std::max_element(qv_y.constBegin(), qv_y.constEnd());
      ui->plotLight->yAxis->setRange(0, y_max * 1.2); // os Y będzie miała wartość większą niż wartość punktu
      ui->plotLight->graph(0)->rescaleAxes(true);
      ui->plotLight->replot();
      ui->plotLight->update();
}

/*!
 * \brief Metoda obsługująca przycisk "Wyczyść"
 * Metoda wywołuje metody clearDateTemp(), plotTemperature(), clearDateLight() oraz plotLight() 
 * w celu usunięcia danych z wykresów temperatury i natężenia światła.
 */

void Charts::on_pushButtonClear_clicked()
{
      clearDateTemp();
      plotTemperature();
      clearDateLight();
      plotLight();
}

/*!
 * \brief Metoda odbierająca dane o temperaturze i dodająca je do wykresu.
 * Metoda wywołuje metodę addPointTemperature() z argumentami x i temperature oraz metodę plotTemperature()
 * w celu dodania punktu na wykresie i aktualizacji go.
 * \param x Czas, w którym zmierzono temperaturę.
 * \param temperature Zmierzona temperatura.
 */

void Charts::receiveDataTemperature(double x, double temperature)
{
    addPointTemperature(x, temperature);
    plotTemperature();
}

/*!
 * \brief Metoda odbierająca dane o natężeniu światła i dodająca je do wykresu.
 * Metoda wywołuje metodę addPointLight() z argumentami x i light oraz metodę plotLight()
 * w celu dodania punktu na wykresie i aktualizacji go.
 * \param x Czas, w którym zmierzono natężenie światła.
 * \param light Zmierzone natężenie światła.
 */

void Charts::receiveDataLight(double x, double light)
{
    addPointLight(x, light);
    plotLight();
}

