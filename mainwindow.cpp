/*!
 * \file mainwindow.cpp
 * \brief Plik z definicjami metod klasy MainWindow
 * Plik zawiera definicje metod klasy MainWindow, która odpowiada za wyswietlanie interfejsu uzytkownika,
 * przetwarzanie danych z portu szeregowego oraz tworzenie tabel.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPixmap>
#include <QApplication>
#include <QColor>
#include <QPainter>
#include <QStatusBar>
#include <QTime>
#include <QDate>
#include <QString>
#include <QFileDialog>
#include <QTime>
#include <QDate>
#include <iostream>
#include <QTranslator>
#include <QEvent>
#include <QRandomGenerator>

/*!
 * \brief Konstruktor głównego okna aplikacji
 * Dodaje elementy do listy rozwijanej wyboru języka. Łączy sygnał zmiany indeksu wyboru języka ze slotem change_language(int).
 * Inicjalizuje etykietę statusu, etykiety odpowiadające za wizualizacje odległości robota od obietków oraz samego robota, 
 * ustwia tabele temperatury, natężenia światła i odległości, mapę teperatury oraz natężenia światła, etykietę robota poruszającego
 * się po mapach oraz etykiety w których prezentowana jest wartość odległości oraz natężenia światła.
 * Inicjalizuje urządznie QSerialPort
 * 
 */
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(QObject::tr("Okno główne"));

    ui->Languages->addItem("Polski");
    ui->Languages->addItem("English");
    connect(ui->Languages, SIGNAL(currentIndexChanged(int)), this, SLOT(change_language(int)));

    setupStatusLabel();
    setupDistanceAndRobotLabel();
    setupTemperatureTable();
    setupLightTable();
   // setupDistanceTable();
    setupTemperatureMap();
    setupLightMap();
    setupRobot();
    setupDistanceAndLightLabels();
    setupLegendTemperature();
    setupLegendLight();
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(on_pushButtonConnection_clicked()));
    timer->start(1000);


    this->device = new QSerialPort(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void  MainWindow::setupLegendTemperature(){
    QPixmap baseTemperature("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/tempBD.png");
    QPixmap cold("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/zimno.png");
    QPixmap verycold("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/bardzo_zimno.png");
    QPixmap lukewarm("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/trochecieplo.png");
    QPixmap hot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/goraco.png");
    QPixmap veryhot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/bardzo_goraco.png");
    QPixmap warm("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/cieplo.png");

    int w=ui->label_min_light->width();
    int h=ui->label_min_light->height();
    ui->label_min_temp->setPixmap(baseTemperature.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep_temp->setPixmap(verycold.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep2_temp->setPixmap(cold.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep3_temp->setPixmap(lukewarm.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep4_temp->setPixmap(warm.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep5_temp->setPixmap(hot.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep6_temp->setPixmap(veryhot.scaled(w, h, Qt::KeepAspectRatio));
}
void  MainWindow::setupLegendLight(){
    QPixmap baseLight("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/lightBD.png");
    QPixmap dark("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/ciemno.png");
    QPixmap dark1("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/bardzo_ciemno.png");
    QPixmap dark2("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/srednio_ciemno.png");
    QPixmap light0("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/jasno.png");
    QPixmap light1("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/bardzo_jasno.png");
    QPixmap light2("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/srednio_jasno.png");

    int w=ui->label_min_light->width();
    int h=ui->label_min_light->height();
    ui->label_min_light->setPixmap(baseLight.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep_light->setPixmap(light2.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep2_light->setPixmap(light1.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep3_light->setPixmap(light0.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep4_light->setPixmap(dark1.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep5_light->setPixmap(dark2.scaled(w, h, Qt::KeepAspectRatio));
    ui->label_minstep6_light->setPixmap(dark.scaled(w, h, Qt::KeepAspectRatio));
}


/*!
 * Ustawienie obrazka na etykiecie statusu. Obraz jest skalowany do wielkości etykiety
 */
void MainWindow::setupStatusLabel(){
    // Ustawienie obrazka na etykiecie labelStatus
    QPixmap start("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/czerwony.png");
    int w=ui->labelStatus->width();
    int h=ui->labelStatus->height();
    ui->labelStatus->setPixmap(start.scaled(w,h,Qt::KeepAspectRatio));
}

/*!
 * Ustawienie obrazków na etykiecie do wizualizacji odległości robota od obiektów. 
 * Ustawia się trzy różene obrazki na jednej etykiecie. Ustawiany jest również
 * obraz robota na drugiej etykiecie. Obrazki są skalowane do wielkości etykiet. 
 */
void MainWindow::setupDistanceAndRobotLabel(){
    int w_d=ui->labelDistance->width()*0.8;
       int h_d=ui->labelDistance->height()*0.8;
     //   ui->labelDistance->move(ui->labelDistance->x(), ui->labelDistance->y() + 20);
       QPixmap robot_start("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/Mobilek_sam.png");
       QPixmap distance_startI("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_I_base.png");
       QPixmap distance_startII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_II_base.png");
       QPixmap distance_startIII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_III_base.png");
       QPixmap scaledDistanceI = distance_startI.scaled(w_d, h_d, Qt::KeepAspectRatio);
       QPixmap scaledDistanceII = distance_startII.scaled(w_d, h_d, Qt::KeepAspectRatio);
       QPixmap scaledDistanceIII = distance_startIII.scaled(w_d, h_d, Qt::KeepAspectRatio);
       QPixmap combinedPixmap(w_d, h_d+20);
       combinedPixmap.fill(Qt::transparent);
       QPainter painter(&combinedPixmap);
       painter.drawPixmap(60, 20, scaledDistanceI);
       painter.drawPixmap(60, 20, scaledDistanceII);
       painter.drawPixmap(60, 20, scaledDistanceIII);

       ui->labelRobot->move(ui->labelRobot->x() + 60, ui->labelRobot->y()+20);
       ui->labelDistance->setPixmap(combinedPixmap);
       ui->labelRobot->setPixmap(robot_start.scaled(w_d,h_d,Qt::KeepAspectRatio));
}

/*!
 * Ustawienie stylu tabeli dla pomiaru natężenia świała.
 * Pierwaszej i ostaniej kolumnie nagłówka ustawiany jest kolor tła zapisany w formie szesnastkowej
 */
void MainWindow::setupTemperatureTable()
{
    ui->tableWidgetTemp->horizontalHeader()->setStyleSheet("QHeaderView::section:first::horizontal, QHeaderView::section:last::horizontal { background-color: #d7d7d7; }");
}

/*!
 * Ustawienie stylu tabeli dla pomiaru temperatury.
 * Dostosowywany jest rozmiar drugiej kolumny do jej zawartości. Pierwaszej i ostaniej kolumnie nagłówka ustawiany jest kolor tła zapisany w formie szesnastkowej
 */
void MainWindow::setupLightTable(){
    ui->tableWidgetLight->resizeColumnToContents(1);
    ui->tableWidgetLight->horizontalHeader()->setStyleSheet("QHeaderView::section:first::horizontal, QHeaderView::section:last::horizontal { background-color: #d7d7d7; }");
}

/*!
 * Ustawiany jest obraz bazowy obszaru mapy temperatury. Obraz jest odpowiednio skalowane do wielkości etykiety. Następnie oblicza się 
 * rozmiar poszczególnych elementów mapy. W pętli są one dodawane do obszaru mapy temperatury.
 */
void MainWindow::setupTemperatureMap(){
    QPixmap mainTemperature("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/Prostokat.png");
    int tempWidth=ui->labelTemperature->width();
    int tempHeight=ui->labelTemperature->height();
    int tempSize;
    if(tempWidth>tempHeight){
        tempSize=tempHeight;
    }
    else{
        tempSize=tempWidth;
    }
    ui->labelTemperature->setPixmap(mainTemperature.scaled(tempSize,tempSize,Qt::KeepAspectRatio));

    QPixmap baseTemperature("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/tempBD.png");
    int x = 276;
    int y = 13;
    int SizeTempBD = (tempSize - 7 * 4) / 7;
    SizeRobot = (((tempSize - 7 * 4) / 6))/2;
    int difference = SizeTempBD + 4;
    QWidget *tab1 = ui->tabWidget->widget(0);

    for(int i = 0; i < 7; i++) {
        for(int j = 0; j < 7; j++) {
            QLabel *myLabel = new QLabel(this);
            myLabel->setPixmap(baseTemperature.scaled(SizeTempBD, SizeTempBD, Qt::KeepAspectRatio));
            myLabel->setGeometry(x + j * difference, y + i * difference, SizeTempBD, SizeTempBD);
            myLabel->setParent(tab1);
            temperatureLabels.append(myLabel); // dodanie QLabel do wektora
        }
    }
}

/*!
 * Ustawiany jest obraz bazowy obszaru mapy natężenia światła. Obraz jest odpowiednio skalowane do wielkości etykiety. 
 * Następnie oblicza się rozmiar poszczególnych elementów mapy. W pętli są one dodawane do obszaru mapy natężenia światła.
 */
void MainWindow::setupLightMap(){
    //Mapa natężenia światła
    QPixmap mainLight("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/Prostokat.png");
    int lightWidth=ui->labelLight->width();
    int lightHeight=ui->labelLight->height();
    int lightSize;
    if(lightWidth>lightHeight){
        lightSize=lightHeight;
    }
    else{
        lightSize=lightWidth;
    }
    ui->labelLight->setPixmap(mainLight.scaled(lightSize,lightSize,Qt::KeepAspectRatio));

    QPixmap baseLight("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/lightBD.png");
    int x_l = 276;
    int y_l = 13;
    SizeLightBD = (lightSize - 7 * 4) / 7;
    int difference_l = SizeLightBD + 4;
    QWidget *tab2 = ui->tabWidget->widget(1);

    for(int i = 0; i < 7; i++) {
        for(int j = 0; j < 7; j++) {
            QLabel *myLabelLight = new QLabel(this);
            myLabelLight->setPixmap(baseLight.scaled(SizeLightBD, SizeLightBD, Qt::KeepAspectRatio));
            myLabelLight->setGeometry(x_l + j * difference_l, y_l + i * difference_l, SizeLightBD, SizeLightBD);
            myLabelLight->setParent(tab2);
            lightLabels.append(myLabelLight); // dodanie QLabel do wektora
        }
    }
}

/*!
 * Ustawiany jest obraz robota na mapach natężenia światła oraz temperatury. Rozmiar obrazka się odpowienio skalowany i dopasowany 
 * do wielkości poszczególnego elementu mapy.
 */
void MainWindow::setupRobot(){
    int lightWidth=ui->labelLight->width();
    int lightHeight=ui->labelLight->height();
    int lightSize;
    if(lightWidth>lightHeight){
        lightSize=lightHeight;
    }
    else{
        lightSize=lightWidth;
    }
    QPixmap robotMove("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/mobilek.png");
    int x_robot=ui->labelLight->x()+(lightSize)/2-SizeRobot/2;
    int y_robot=ui->labelLight->y()+(lightSize)/2-SizeRobot/2;
    robotLabel = new QLabel(ui->tab_2);
    robotLabelTemp =new QLabel(ui->tab);
    robotLabel->setPixmap(robotMove.scaled(SizeRobot, SizeRobot, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    robotLabelTemp->setPixmap(robotMove.scaled(SizeRobot, SizeRobot, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    robotLabel->setGeometry(x_robot, y_robot, SizeRobot, SizeRobot);
    robotLabelTemp->setGeometry(x_robot, y_robot, SizeRobot, SizeRobot);
}

/*!
 * Inicjalizuje etykiety związane z odległością i natężeniem światła w interfejsie. Tworzy i ustawia etykiety 
 * w odpowiednich miejscach w interfejsie, prezentując informacje o odległości i natężeniu światła.
 */
void MainWindow::setupDistanceAndLightLabels(){
    int w_d=ui->labelDistance->width()*0.8;
    int h_d=ui->labelDistance->height()*0.8;
    double distance=0.0;
    label_I = new QLabel(ui->groupBoxDistance);
    label_I->setGeometry(ui->labelRobot->x()+w_d/2-50, ui->labelRobot->y()-30, 50, 25);
    label_I->setText(QString("%1").arg(distance));
    label_I->setStyleSheet("QLabel { border: 1px solid black; padding: 2px; background-color: rgb(255, 255, 255) }");

    label_II = new QLabel(ui->groupBoxDistance);
    label_II->setGeometry(ui->labelRobot->x()-55, ui->labelRobot->y()+h_d/2, 50, 25);
    label_II->setText(QString("%1").arg(distance));
    label_II->setStyleSheet("QLabel { border: 1px solid black; padding: 2px; background-color: rgb(255, 255, 255) }");

    label_III = new QLabel(ui->groupBoxDistance);
    label_III->setGeometry(ui->labelRobot->x()+w_d-50, ui->labelRobot->y()+h_d/2, 50, 25);
    label_III->setText(QString("%1").arg(distance));
    label_III->setStyleSheet("QLabel { border: 1px solid black; padding: 2px; background-color: rgb(255, 255, 255) }");

    /*----------LIGHT LABEL------------*/

    light=new QLabel(ui->groupBoxDistance);
    light->setGeometry(ui->labelRobot->x()+w_d/2-100,  ui->labelRobot->y()+h_d-10, 60, 60);
    QPixmap light_icon("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Icon/light_icon.png");
    int w_l=light->width();
    int h_l=light->height();
    light->setPixmap(light_icon.scaled(w_l,h_l,Qt::KeepAspectRatio, Qt::SmoothTransformation));

    double light_value=0.0;
    light_intesity=new QLabel(ui->groupBoxDistance);
    light_intesity->setGeometry(ui->labelRobot->x()+w_d/2-50,  ui->labelRobot->y()+h_d+10, 65, 30);
    light_intesity->setText(QString("%1").arg(light_value));
    light_intesity->setStyleSheet("QLabel { border: 1px solid black; padding: 2px; background-color: rgb(255, 255, 255) }");
}

/*!
 * Wywoływana po klinknięciu przycisku "Wyczyść". Metoda ta usuwa wszystkie dane znajdujące się w komórkach tabeli, zachowując nagłówki.
 * Ustawia liczbę wierszy tabeli na zero, czyli usuwa wszystkie wiersze z tabeli.
 * 
 */
void MainWindow::on_pushButtonClear_clicked()
{
    ui->tableWidgetTemp->clearContents();
    ui->tableWidgetTemp->setRowCount(0);

    ui->tableWidgetLight->clearContents();
    ui->tableWidgetLight->setRowCount(0);
}

/*!
 * Służy do dodawania danych temperatury do tabeli tableWidgetTemp w interfejsie głównego okna. Sprawdza czy tabela ma więcej niż 10 wierszy, jeżeli
 * ma to usuwa najstarszy wiersz. Dodaje kolejne dane na koniec tabeli. Nowe wiersze zawierają aktualny czas oraz wartość temperatury w stopniach Celcjusza.
 * \param[in] temperature - wartość odczytanej z urządznia temperatury w stopniach Celcjusza
 */
void MainWindow::addToTableTemp(double temperature)
{

    ui->labelTemp_param->setText(QString("%1").arg(temperature));
    int rowCount=ui->tableWidgetTemp->rowCount();
    if(rowCount>9){
        ui->tableWidgetTemp->removeRow(0);
        rowCount--;
    }

    ui->tableWidgetTemp->insertRow(rowCount);

    ui->tableWidgetTemp->setItem(rowCount, TIMETEMP, new QTableWidgetItem(QTime::currentTime().toString()));
    ui->tableWidgetTemp->setItem(rowCount, TEMPERATURE, new QTableWidgetItem(QString::number(temperature)));
}

/*!
 * Służy do dodawania danych natężenia światła do tabeli tableWidgetLight w interfejsie głównego okna. Sprawdza czy tabela ma więcej niż 10 wierszy, jeżeli
 * ma to usuwa najstarszy wiersz. Dodaje kolejne dane na koniec tabeli. Nowe wiersze zawierają aktualny czas oraz wartość natężenia światła w luksach.
 * \param[in] light - wartość odczytanego z urządznia natężenia światła w luksach
 */
void MainWindow::addToTableLight(double light)
{
    ui->labelLight_param->setText(QString("%1").arg(light));
    int rowCount = ui->tableWidgetLight->rowCount();
    if(rowCount>9){
        ui->tableWidgetLight->removeRow(0);
        rowCount--;
    }
    ui->tableWidgetLight->insertRow(rowCount);
    ui->tableWidgetLight->setItem(rowCount, TIMELIGHT, new QTableWidgetItem(QTime::currentTime().toString()));
    ui->tableWidgetLight->setItem(rowCount, INTENSITY, new QTableWidgetItem(QString::number(light)));
}

/*!
 *  Służy do aktualizacji etykiet odległości w interfejsie głównego okna na podstawie wartości odległości przekazanych jako argumenty. 
 * Dostosowywana jest wielkość obrazków do wielkości etykiety. W zależności od wartości odległości (distance_I, distance_II, distance_III), 
 * wybiany jest odpowiedni obrazek (blisko, średnio, daleko, bardzo daleko) dla każdej strefy i wysowany na combinedPixmap.
 * \param[in] distance_I - wartość odległości odczytanej z urządznia odpowiadającej odległosci robota od obietków w centymetrach w I strefie
 * \param[in] distance_II - wartość odległości odczytanej z urządznia odpowiadającej odległosci robota od obietków w centymetrach w II strefie
 * \param[in] distance_III - wartość odległości odczytanej z urządznia odpowiadającej odległosci robota od obietków w centymetrach w III strefie
 */
void MainWindow::updateDistanceLabel(double distance_I, double distance_II, double distance_III)
{
    int w_d=ui->labelDistance->width()*0.8;
    int h_d=ui->labelDistance->height()*0.8;
    label_I->setText(QString("%1").arg(distance_I));
    label_II->setText(QString("%1").arg(distance_II));
    label_III->setText(QString("%1").arg(distance_III));

    QPixmap distance_startI("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_I_base.png");
    QPixmap distance_startII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_II_base.png");
    QPixmap distance_startIII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_III_base.png");
    QPixmap scaledDistanceI = distance_startI.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceII = distance_startII.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceIII = distance_startIII.scaled(w_d, h_d, Qt::KeepAspectRatio);

    QPixmap distance_closeI("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_I_close.png");
    QPixmap distance_mediumI("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_I_medium.png");
    QPixmap distance_far_awayI("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_I_far.png");
    QPixmap distance_very_farI("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_I_base.png");

    QPixmap scaledDistanceIclose = distance_closeI.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceImedium = distance_mediumI.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceIfar_away = distance_far_awayI.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceIveryfar = distance_very_farI.scaled(w_d, h_d, Qt::KeepAspectRatio);

    /*-----------II---------------*/
    QPixmap distance_closeII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_II_close.png");
    QPixmap distance_mediumII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_II_medium.png");
    QPixmap distance_far_awayII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_II_far.png");
    QPixmap distance_very_farII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_II_base.png");

    QPixmap scaledDistanceIIclose = distance_closeII.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceIImedium = distance_mediumII.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceIIfar_away = distance_far_awayII.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceIIveryfar = distance_very_farII.scaled(w_d, h_d, Qt::KeepAspectRatio);

    /*-----------III---------------*/
    QPixmap distance_closeIII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_III_close.png");
    QPixmap distance_mediumIII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_III_medium.png");
    QPixmap distance_far_awayIII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_III_far.png");
    QPixmap distance_very_farIII("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Odleglosc1/strefa_III_base.png");

    QPixmap scaledDistanceIIIclose = distance_closeIII.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceIIImedium = distance_mediumIII.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceIIIfar_away = distance_far_awayIII.scaled(w_d, h_d, Qt::KeepAspectRatio);
    QPixmap scaledDistanceIIIveryfar = distance_very_farIII.scaled(w_d, h_d, Qt::KeepAspectRatio);


    QPixmap combinedPixmap(w_d, h_d+20);
    combinedPixmap.fill(Qt::transparent);
    QPainter painter(&combinedPixmap);
    painter.drawPixmap(60, 20, scaledDistanceI);
    painter.drawPixmap(60, 20, scaledDistanceII);
    painter.drawPixmap(60, 20, scaledDistanceIII);
    ui->labelDistance->setPixmap(combinedPixmap);
    double min=18.0;
    double max=30.0;
    min=ui->doubleSpinBoxDistance_MIN->value();
    max=ui->doubleSpinBox_Distance_Max->value();
    double step=(max-min)/2.0;

    if(distance_I<min){
        painter.drawPixmap(60 , 20, scaledDistanceIclose);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    else if(distance_I>=min&&distance_I<=min+step){
        painter.drawPixmap(60 , 20, scaledDistanceImedium);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    else if(distance_I>min+step&&distance_I<min+step*2){
        painter.drawPixmap(60 , 20, scaledDistanceIfar_away);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    else{
        painter.drawPixmap(60 , 20, scaledDistanceIveryfar);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    /*---2------*/
    if(distance_II<min){
        painter.drawPixmap(60 , 20, scaledDistanceIIclose);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    else if(distance_II>=min&&distance_II<=min+step){
        painter.drawPixmap(60 , 20, scaledDistanceIImedium);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    else if(distance_II>min+step&&distance_II<min+step*2){
        painter.drawPixmap(60 , 20, scaledDistanceIIfar_away);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    else{
        painter.drawPixmap(60 , 20, scaledDistanceIIveryfar);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    /*---3------*/
    if(distance_III<min){
        painter.drawPixmap(60 , 20, scaledDistanceIIIclose);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    else if(distance_III>=min&&distance_III<=min+step){
        painter.drawPixmap(60 , 20, scaledDistanceIIImedium);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    else if(distance_III>min+step&&distance_III<min+step*2){
        painter.drawPixmap(60 , 20, scaledDistanceIIfar_away);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
    else{
        painter.drawPixmap(60 , 20, scaledDistanceIIIveryfar);
        ui->labelDistance->setPixmap(combinedPixmap);
    }
}


void MainWindow::on_pushButtonAdd_clicked()
{
    clickCounter=1;
}


void MainWindow::on_pushButtonChart_clicked()
{
    chart = new Charts(this);
    chart->show();
    x=0.0;
}

/*!
 * Odpowiada za odczyt danych z portu oraz aktualizację danych w metodach na podstawie otrzymanych informacji.
 * Sprawdzenie warunku, czy nie zostały jeszcze odczytane dane i czy urządzenie może odczytać linię. Odbiór danych jest opóźniany
 * i wywoływany co 100 iteracje. Odpowiada za: wywołanie metody updateDistanceLabel w celu aktualizacji etykiety odległości na 
 * podstawie wartości distance_I, distance_II i distance_III, wywołanie metody addToTableLight w celu dodania wartości 
 * light_intensity do tabeli w interfejsie, wywołanie metody addToTableTemp w celu dodania wartości temperature do tabeli w interfejsie, 
 * wywołanie metody updateLightRobotLabel w celu aktualizacji etykiety z informacją o natężeniu światła, wywołanie metody changeColorLight 
 * w celu zmiany koloru w zależności od natężenia światła, wywołanie metody changeColorTemperature w celu zmiany koloru w zależności od temperatury. 
 * Emituje sygnały sendTemperature i sendLight z odpowiednimi wartościami x (czas w sekundach), temperature (wartość temperatury w stopniach Celcjusza)
 * i light_intensity (wartość natężenia światła w luksach).
 * 
 */
void MainWindow::readFromPort()
{
    double distance_I=0.0;
    double distance_II=0.0;
    double distance_III=0.0;
    double temperature=0.0;
    double light_intensity=0.0;
    isDataRead = false;

    if (!isDataRead &&this->device->canReadLine()) {
        secCounter++;

        if(secCounter % 100 == 0) {

        if(this->device->canReadLine()){
        QByteArray receivedData = device->readLine().trimmed();

        // Sprawdzenie, czy pierwszy element to "RM"
        QList<QByteArray> dataElements = receivedData.split(' ');
        if (!dataElements.isEmpty() && dataElements.first() == "RM") {
            qDebug() << "Received frame: " << receivedData;

            secCounter=1;
            x++;
            bool ok_I;
            bool ok_II;
            bool ok_III;
            bool ok_light;
            bool ok_temperature;
            bool ok_left;
            bool ok_right;
            distance_II = dataElements.value(1).toDouble(&ok_II);
            distance_I = dataElements.value(2).toDouble(&ok_I);
            distance_III = dataElements.value(3).toDouble(&ok_III);
            light_intensity=dataElements.value(4).toDouble(&ok_light);
            temperature=dataElements.value(5).toDouble(&ok_temperature);
            lewo_przod=dataElements.value(6).toDouble(&ok_left);
            prawo_przod=dataElements.value(7).toDouble(&ok_right);

            this->updateDistanceLabel(distance_I, distance_II, distance_III);
            this->addToTableLight(light_intensity);
            this->addToTableTemp(temperature);
            this->updateLightRobotLabel(light_intensity);
            this->changeColorLight(light_intensity);
            this->changeColorTemperature(temperature);
            emit sendTemperature(x, temperature);
            emit sendLight(x, light_intensity);
        }else{
            secCounter=99;
            this->device->readLine();
        }
        }
        else{
            secCounter=99;
            this->device->readLine();
        }
        }
        this->device->readLine();

    }
}

uint16_t calculateCRC(const unsigned char* data_p, unsigned char length){
    unsigned char x;
    unsigned short crc = 0xFFFF;

    while(length--) {
        x = crc >> 8 ^ *data_p++;
        x ^= x>>4;
        crc = (crc << 8) ^ ((unsigned short)(x << 12)) ^ ((unsigned short)(x <<5)) ^ ((unsigned short)x);
    }

    return crc;
}

/*!
 * Odpowiada za obsługę zdarzenia kliknięcia przycisku "Połączenie" w interfejsie aplikacji. 
 * Funkcja jest wywoływana po kliknięciu tego przycisku i wykonuje operacje związane z nawiązaniem połączenia z urządzeniem.
 * Poprawne połącznie sygnalizowane jest zmianą obrazka na etykiecie labelStatus. Inicjalizuje listę devices przechowującą 
 * informacje o dostępnych portach szeregowych. Sprawdza warunki, czy znaleziono co najmniej 2 urządzenia i czy pierwsze z nich 
 * ma nazwę "ttyACM0". Jeśli warunek jest spełniony, pobranie nazwy portu i ustawienie jej w obiekcie device. Jeśli otwarcie portu 
 * się powiedzie, następuje konfiguracja parametrów portu (szybkość transmisji, liczba bitów danych, parzystość, bity stopu, 
 * kontrola przepływu), czyszczenie bufora portu. Łączy sygnały readyRead() obiektu device z metodą readFromPort() w celu odczytu 
 * danych z portu szeregowego, errorOccurred(QSerialPort::SerialPortError) obiektu device z metodą handleError(QSerialPort::SerialPortError) 
 * w celu obsługi błędów portu szeregowego. Jeśli otwarcie portu się nie powiedzie wyświetla informację diagnostyczną o nieudanej próbie 
 * połączenia z urządzeniem oraz wyświetla komunikat błędu za pomocą QMessageBox::critical(). 
 */ 
/*
void MainWindow::on_pushButtonConnection_clicked()
{
    QPixmap connected("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/zielony.png");
    qDebug()<<QObject::tr("Szukam urządzeń...");
    QList<QSerialPortInfo> devices;
    devices = QSerialPortInfo::availablePorts();
    for(int i = 0; i < devices.count(); i++) {
        qDebug() <<"Znalazłem urządzenie: " << devices.at(i).portName() << " " << devices.at(i).description();
    }
    if(devices.count() >= 2 && devices.at(0).portName() == "ttyACM0") {
        QString portName = devices.at(0).portName();
        this->device->setPortName(portName);
        qDebug() << QObject::tr("Nazwa portu: ") << portName;
    } else {
        qDebug() << QObject::tr("Nie znaleziono urządzenia STMicroelectronics STLink Virtual COM Port.");
    }

    if(!device->isOpen()){
    if(device->open(QSerialPort::ReadWrite)) {
      this->device->setBaudRate(QSerialPort::Baud115200);
      this->device->setDataBits(QSerialPort::Data8);
      this->device->setParity(QSerialPort::NoParity);
      this->device->setStopBits(QSerialPort::OneStop);
      this->device->setFlowControl(QSerialPort::NoFlowControl);
      this->device->clear();
      qDebug()<<QObject::tr("Otwarto port szeregowy.");
      int w=ui->labelStatus->width();
      int h=ui->labelStatus->height();
      ui->labelStatus->setPixmap(connected.scaled(w,h,Qt::KeepAspectRatio));
      connect(this->device, SIGNAL(readyRead()), this, SLOT(readFromPort()));
      connect(this->device, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this, SLOT(handleError(QSerialPort::SerialPortError)));
    } else {
      qDebug()<<QObject::tr("Otwarcie porty szeregowego się nie powiodło!");
      QMessageBox::critical(this, QObject::tr("Błąd"), QObject::tr("Nie udało się połączyć z urządzeniem."));
      return;
    }
    }
    else{
    qDebug()<<QObject::tr("Port już jest otwarty!");
         return;
    }
}*/

void MainWindow::on_pushButtonConnection_clicked(){
    QPixmap connected("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/zielony.png");
    int w=ui->labelStatus->width();
    int h=ui->labelStatus->height();
    ui->labelStatus->setPixmap(connected.scaled(w,h,Qt::KeepAspectRatio));
    QRandomGenerator* generator = QRandomGenerator::global();

   // int randomNumber = generator->bounded(-5, 11);
    double randomDouble = generator->generateDouble();  // Generowanie losowej liczby zmiennoprzecinkowej z zakresu [0, 1)
    double randomDoubleII = generator->generateDouble();
    double randomDoubleIII = generator->generateDouble();
     // Przekształć liczbę do zakresu (-5, 10)
     double transformedDoubleI = -5.0 + (randomDouble * 10.0);
     double randomNumberI = QString::number(transformedDoubleI, 'f', 1).toDouble();

     double transformedDoubleII = -5.0 + (randomDoubleII * 10.0);
     double randomNumberII = QString::number(transformedDoubleII, 'f', 1).toDouble();
     double transformedDoubleIII = -5.0 + (randomDoubleIII * 10.0);
     double randomNumberIII = QString::number(transformedDoubleIII, 'f', 1).toDouble();


    // int randomNumber = generator->bounded(-5, 11);
      double randomDoubleTemp = generator->generateDouble();  // Generowanie losowej liczby zmiennoprzecinkowej z zakresu [0, 1)

      double transformedDoubleTemp = -1.0 + (randomDouble * 2.0);
      double randomNumberTemp = QString::number(transformedDoubleTemp, 'f', 1).toDouble();

      double randomDoubleLight = generator->generateDouble();  // Generowanie losowej liczby zmiennoprzecinkowej z zakresu [0, 1)

      double transformedDoubleLight = -100.0 + (randomDouble * 200.0);
      double randomNumberLight = QString::number(transformedDoubleLight, 'f', 1).toDouble();

      int randomNumberI_speed = generator->bounded(-5, 11);

      int randomNumberII_speed = generator->bounded(-5, 11);


            secCounter=1;
            x++;

            distance_II = distance_II+randomNumberII;
            if(distance_II<2.0){
                distance_II=2.0;
            }
            distance_I = distance_I+randomNumberI;
            if(distance_I<2.0){
                distance_I=2.0;
            }
            distance_III = distance_III+randomNumberIII;
            if(distance_III<2.0){
                distance_III=2.0;
            }
            light_intensity=light_intensity+randomNumberLight;
            if(light_intensity<0){
                light_intensity=0.0;
            }
            temperature=temperature+randomNumberTemp;
            if(temperature<20){
                temperature=temperature+6;
            }
            lewo_przod=lewo_przod+randomNumberI_speed;
            prawo_przod=prawo_przod+randomNumberII_speed;

            this->updateDistanceLabel(distance_I, distance_II, distance_III);
            this->addToTableLight(light_intensity);
            this->addToTableTemp(temperature);
            this->updateLightRobotLabel(light_intensity);
            this->changeColorLight(light_intensity);
            this->changeColorTemperature(temperature);
            emit sendTemperature(x, temperature);
            emit sendLight(x, light_intensity);
}

/*!
 * Wywoływana w przypadku wystąpienia błędu związanych z portem szeregowym. Jej zadaniem jest obsłużyć ten błąd i podjąć odpowiednie działania.
 * Utracenie połączenia powoduje zmianę obrazka na etykiecie labelStatus. Sprawdza warunek czy wystąpił błąd zasobu portu szeregowego.
 * Jeśli warunek jest spełniony, wyświetlenie informacji diagnostycznej o utracie połączenia z urządzeniem. Zamyka port szeregowy poprzez 
 * wywołanie metody close() na obiekcie device.
 * 
 */ 
void MainWindow::handleError(QSerialPort::SerialPortError error)
{
    QPixmap disconnected("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/czerwony.png");
    if (error == QSerialPort::ResourceError) {
         qDebug() << QObject::tr("Utracono połączenie z urządzeniem!");
         int w=ui->labelStatus->width();
         int h=ui->labelStatus->height();
         ui->labelStatus->setPixmap(disconnected.scaled(w,h,Qt::KeepAspectRatio));
         this->device->close();
    }
}

/*!
 * Odpowiedzialna za zmianę koloru etykiety labelTemperature w zależności od odczytanej wartości temperatury.
 * Obliczeny jest rozmiar ikony temperatury na podstawie rozmiaru etykiety. Rozmiar ten jest obliczany na podstawie 
 * ilości etykiet temperatury i dostępnej przestrzeni. Porównanie wartości temperatury z określonymi przedziałami, 
 * a następnie ustawienie odpowiedniej ikony temperatury dla etykiety labelTemperature. Uzyskuje odpowiedni 
 * wskaźnik QLabel na podstawie aktualnej pozycji robota (robotX i robotY) i pobiera etykiety temperatury z wektora 
 * temperatureLabels.
 * \param[in] temperature - wartość odczytanej z urządznia temperatury w stopniach Celcjusza
 */ 


void MainWindow::changeColorTemperature(double temperature){
    int tempWidth=ui->labelTemperature->width();
    int tempHeight=ui->labelTemperature->height();
    int tempSize;
    if(tempWidth>tempHeight){
         tempSize=tempHeight;
    }
    else{
         tempSize=tempWidth;
    }
    int SizeTempBD = (tempSize - 7 * 4) / 7;
    QPixmap cold("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/zimno.png");
    QPixmap verycold("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/bardzo_zimno.png");
    QPixmap lukewarm("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/trochecieplo.png");
    QPixmap hot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/goraco.png");
    QPixmap veryhot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/bardzo_goraco.png");
    QPixmap warm("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/cieplo.png");
    double min=18.0;
    double max=30.0;
    min=ui->doubleSpinBox_Temperature_MIN->value();
    max=ui->doubleSpinBox_Temperature_MAX->value();
    double step=(max-min)/4.0;

    if(temperature<min){
         QLabel* myLabelT = temperatureLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(verycold.scaled(SizeTempBD, SizeTempBD, Qt::KeepAspectRatio));
    }
    if(temperature>=min&&temperature<min+step){
         QLabel* myLabelT = temperatureLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(cold.scaled(SizeTempBD, SizeTempBD, Qt::KeepAspectRatio));
    }
    if(temperature>=min+step&&temperature<min+2*step){
         QLabel* myLabelT = temperatureLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(lukewarm.scaled(SizeTempBD, SizeTempBD, Qt::KeepAspectRatio));
    }
    if(temperature>=min+2*step&&temperature<min+3*step){
         QLabel* myLabelT = temperatureLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(warm.scaled(SizeTempBD, SizeTempBD, Qt::KeepAspectRatio));
    }
    if(temperature>=min+3*step&&temperature<min+4*step){
         QLabel* myLabelT = temperatureLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(hot.scaled(SizeTempBD, SizeTempBD, Qt::KeepAspectRatio));
    }
    if(temperature>=min+4*step){
         QLabel* myLabelT = temperatureLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(veryhot.scaled(SizeTempBD, SizeTempBD, Qt::KeepAspectRatio));
    }


}


void MainWindow::updateLightRobotLabel(double light) {
    light_intesity->setText(QString("%1").arg(light));
    int w_d = ui->labelDistance->width()*0.8;
    int h_d = ui->labelDistance->height()*0.8;
    QPixmap dark_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_ciemno.png");
    QPixmap lightI_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_jasno_I.png");
    QPixmap lightII_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_jasno_II.png");
    QPixmap lightIII_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_jasno_III.png");
    QPixmap lightIV_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_jasno_IV.png");
    QPixmap lightV_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_jasno_V.png");
    QPixmap lightVI_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_jasno_VI.png");
    QPixmap lightVII_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_jasno_VII.png");
    QPixmap lightVIII_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_jasno_VIII.png");
    QPixmap lightIX_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_jasno_IX.png");
    QPixmap lightX_robot("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Robot/base_mobilek_jasno_X.png");
    double min=18.0;
    double max=30.0;
    min=ui->doubleSpinBox_Light_MIN->value();
    max=ui->doubleSpinBox_Light_MAX->value();
    double step=(max-min)/9.0;
    if(light<min){

         ui->labelRobot->setPixmap(dark_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }
    if(light>=min&&light<min+step){
        ui->labelRobot->setPixmap(lightI_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }
    if(light>=min+step&&light<min+2*step){
        ui->labelRobot->setPixmap(lightII_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }

    if(light>=min+2*step&&light<min+3*step){
        ui->labelRobot->setPixmap(lightIII_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }

    if(light>=min+3*step&&light<min+4*step){
        ui->labelRobot->setPixmap(lightIV_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }
    if(light>=min+4*step&&light<min+5*step){

         ui->labelRobot->setPixmap(lightV_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }

    if(light>=min+5*step&&light<min+6*step){

         ui->labelRobot->setPixmap(lightVI_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }

    if(light>=min+6*step&&light<min+7*step){

         ui->labelRobot->setPixmap(lightVII_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }

    if(light>=min+7*step&&light<min+8*step){

         ui->labelRobot->setPixmap(lightVIII_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }

    if(light>=min+8*step &&light<min+9*step){

         ui->labelRobot->setPixmap(lightIX_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }
    if(light>=min+9*step){

         ui->labelRobot->setPixmap(lightX_robot.scaled(w_d,h_d,Qt::KeepAspectRatio));
    }

}
float MainWindow::calculateAngle() {
   // const float linearSlope = 1.81;
    const float linearSlope = 0.9;
    float difference = qAbs(prawo_przod - lewo_przod);

    if (qAbs(qAbs(prawo_przod) - qAbs(lewo_przod)) > 2) {
        if (prawo_przod > lewo_przod) {
            angle = -difference * linearSlope;
            current_angle += angle;
            if (current_angle <= -360) {
                angle = 0.0;
            }
        }
        else if (prawo_przod < lewo_przod) {
            angle = difference * linearSlope;
            current_angle += angle;
            if (current_angle >= 360) {
                angle = 0.0;
            }
        }
    }
    else {
        angle = 0.0;
    }

    if (current_angle >= 360 || current_angle <= -360) {
        current_angle = 0.0;
    }

    return current_angle;
}

int calculateLightSize(int width, int height) {
    return (width > height) ? height : width;
}

QVector2D calculateVehicleSpeed(double prawo_przod, double lewo_przod) {
    double vehicleSpeedX = (prawo_przod - lewo_przod) / 2;
    double vehicleSpeedY = (prawo_przod + lewo_przod) / 2;
    return QVector2D(vehicleSpeedX, -vehicleSpeedY);
}

QPixmap rotatePixmap(const QPixmap& pixmap, float angle) {
    QPixmap rotatedPixmap(pixmap.size());
    rotatedPixmap.fill(Qt::transparent);
    QPainter painter(&rotatedPixmap);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
    painter.translate(pixmap.width() / 2, pixmap.height() / 2);
    painter.rotate(angle);
    painter.translate(-pixmap.width() / 2, -pixmap.height() / 2);
    painter.drawPixmap(0, 0, pixmap);
    return rotatedPixmap;
}

void calculateNewPosition(QVector2D przesuniecie, float angle, int currentX, int currentY, int x_robot, int y_robot, int lightSize, int& newX, int& newY) {
    float angleRadians = qDegreesToRadians(angle);
    float cosAngle = qCos(angleRadians);
    float sinAngle = qSin(angleRadians);
    float deltaX = przesuniecie.x() * cosAngle - przesuniecie.y() * sinAngle;
    float deltaY = przesuniecie.x() * sinAngle + przesuniecie.y() * cosAngle;
    newX = currentX + deltaX;
    newY = currentY + deltaY;

    if (newX < x_robot - 10 || newX >= x_robot + lightSize - 10 || newY < y_robot - 10 || newY >= y_robot + lightSize - 10) {
        newX = currentX - deltaX;
        newY = currentY - deltaY;
        qDebug() << newX;
        qDebug() << newY;
    }
}


void MainWindow::changeColorLight(double light){
    int lightWidth=ui->labelLight->width();
    int lightHeight=ui->labelLight->height();
    int lightSize=calculateLightSize(lightWidth, lightHeight);

    int czas=1;
    QVector2D vehicleVelocity2=calculateVehicleSpeed(prawo_przod, lewo_przod);
    QVector2D przesuniecie = vehicleVelocity2 * czas/10;

    QPixmap vehicle("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/mobilek.png");
    QPixmap scaledPixmap=vehicle.scaled(SizeRobot, SizeRobot, Qt::KeepAspectRatio, Qt::SmoothTransformation);

    calculateAngle();
    int x_robot=ui->labelLight->x()-SizeRobot/2;
    int y_robot=ui->labelLight->y()+SizeRobot/2;
    int currentX=robotLabel->x();
    int currentY=robotLabel->y();
    int newX;
    int newY;
    calculateNewPosition(przesuniecie, current_angle, currentX, currentY, x_robot, y_robot, lightSize, newX, newY);

    QPixmap rotatedPixmap = rotatePixmap(scaledPixmap, current_angle);
    robotLabel->setPixmap(rotatedPixmap);
    robotLabelTemp->setPixmap(rotatedPixmap);


    robotLabel->move(newX, newY);
    robotLabelTemp->move(newX, newY);
    int robot_x = robotLabel->x();
    int robot_y = robotLabel->y();
    robotX=static_cast<int>(std::round((robot_x+(SizeRobot/2))/SizeLightBD)-6);
    robotY=static_cast<int>(std::round((robot_y-(SizeRobot/2))/SizeLightBD));
    if(robotX<0){
         robotX=0;
    }
    if(robotY<0){
         robotY=0;
    }
    if(robotX>6){
         robotX=6;
    }
    if(robotY>6){
         robotY=6;
    }
    qDebug()<<"X: "<<robotX;
    qDebug()<<"Y: "<<robotY;

// -----------------------------------------------------------------------------------------

    int SizeLightBD = (lightSize - 7 * 4) / 7;
    QPixmap dark("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/ciemno.png");
    QPixmap dark1("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/bardzo_ciemno.png");
    QPixmap dark2("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/srednio_ciemno.png");
    QPixmap light0("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/jasno.png");
    QPixmap light1("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/bardzo_jasno.png");
    QPixmap light2("/home/zk/Pulpit/Aplikacja_WDS/Aplikacja_obrazki/Status/srednio_jasno.png");
    double min=0.0;
    double max=3000.0;
    min=ui->doubleSpinBox_Light_MIN->value();
    max=ui->doubleSpinBox_Light_MAX->value();
    double step=(max-min)/4.0;

    if(light<min){
         QLabel* myLabelT = lightLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(dark1.scaled(SizeLightBD, SizeLightBD, Qt::KeepAspectRatio));
    }
    if(light>=min&&light<min+step){
         QLabel* myLabelT = lightLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(dark.scaled(SizeLightBD, SizeLightBD, Qt::KeepAspectRatio));
    }
    if(light>=min+step&&light<min+step*2){
         QLabel* myLabelT = lightLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(dark2.scaled(SizeLightBD, SizeLightBD, Qt::KeepAspectRatio));
    }

    if(light>=min+step*2&&light<min+step*3){
         QLabel* myLabelT = lightLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(light2.scaled(SizeLightBD, SizeLightBD, Qt::KeepAspectRatio));
    }

    if(light>=min+step*3&&light<min+step*4){
         QLabel* myLabelT = lightLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(light0.scaled(SizeLightBD, SizeLightBD, Qt::KeepAspectRatio));
    }
    if(light>=min+step*4){
         QLabel* myLabelT = lightLabels[robotY * 7 + robotX]; // pobranie QLabel z wektora
         myLabelT->setPixmap(light1.scaled(SizeLightBD, SizeLightBD, Qt::KeepAspectRatio));
    }
}


void MainWindow::change_language(int index)
{
    static QTranslator *t= new QTranslator();
    QString language = ui->Languages->currentText();
    qDebug()<<"Wchodze "<<language;
    if (index == 1)
    {
         qApp->removeTranslator(t);
         if(t->load(":/WDS_aplikacja_Qt_en_EN.qm")){
            qApp->installTranslator(t);
         }
         else{
                qDebug()<<"Nie znaleziono EN";
         }
      ui->Languages->setCurrentIndex(1);

         qDebug()<<"Angielski";

    }
    else if (index == 0)
    {
         qApp->removeTranslator(t);
         if( t->load(":/WDS_aplikacja_Qt_pl_PL.qm")){
      qApp->installTranslator(t);}
         else{
      qDebug()<<"Nie znaleziono PL";
         }
         ui->Languages->setCurrentIndex(0);
    }
    ui->retranslateUi(this);
}
