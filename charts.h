/*!
 * \file charts.h
 * \brief Plik nagłówkowy klasy Charts
 * 
 * Plik nagłówkowy zawiera deklarację klasy Charts oraz jej metod i slotów.
 */

#ifndef CHARTS_H
#define CHARTS_H

#include <QDialog>
#include <QtCharts>
#include <QChartView>
#include <QLineSeries>
#include <QSerialPort>
#include <QPoint>


namespace Ui {
class Charts;
}

/*!
 * \class Charts
 * \brief Klasa odpowidająca za wyświetlanie wykresu temperatury oraz wykresu natężenia światła.
 * 
 * Klasa Charts odpowiada za wyświetlanie wykresów temperatury i natężenia światła  w interfejsie użytkownika.
 * Wykresy są generowane na podstawie danych odebranych z zewnętrznego urządzenia.
 */

class Charts : public QDialog
{
    Q_OBJECT

public:

/*!
 * \brief Konstruktor klasy Charts
 * \param[in] parent - wskaźnik na rodzica
 */
    explicit Charts(QWidget *parent = nullptr);

/*!
 * \brief Destruktor klasy Charts
 */    
    ~Charts();
/*!
 * \brief Dodaje punkt na wykresie natężenia światła
 * \param[in] x - wartość na osi X odpowiadająca czasowi w sekundach
 * \param[in] y - wartość na osi Y odpowiadająca wartości natężenia światła w luksach
 */
    void addPointLight(double x, double y);
/*!
 * \brief Dodaje punkt na wykresie natężenia światła
 * \param[in] x - wartość na osi X odpowiadająca czasowi w sekundach
 * \param[in] y - wartość na osi Y odpowiadająca wartości temperatury w stopniach Celsjusza
 */
    void addPointTemperature(double x, double y);
/*!
 * \brief Czyści dane na wykresie temperatury
 */
    void clearDateTemp();
/*!
 * \brief Czyści dane na wykresie natężenia światła
 */
    void clearDateLight();
/*!
 * \brief Generuje wykresy
 */
    void plot();
/*!
 * \brief Generuje wykres temperatury
 */
    void plotTemperature();
/*!
 * \brief Generuje wykres natężenia światła
 */
    void plotLight();

private slots:
/*!
 * \brief Obsługuje kliknięcie przycisku "Wyczyść"
 */
    void on_pushButtonClear_clicked();
/*!
 * \brief Odbiera dane o temperaturze i dodaje je do wykresu temperatury
 * \param[in] x - wartość na osi X odpowiadająca czasowi w sekundach
 * \param[in] temperature - wartość na osi Y odpowiadająca wartości temperatury w stopniach Celsjusza
 */
    void receiveDataTemperature(double x, double temperature);
/*!
 * \brief Odbiera dane o temperaturze i dodaje je na wykres temperatury
 * \param[in] x - wartość na osi X odpowiadająca czasowi w sekundach
 * \param[in] light - wartość na osi Y odpowiadająca wartości natężenia światła w luksach
 */
    void receiveDataLight(double x, double light);

private:
/*!
 * \brief Obiekt interfejsu użytkownika
 */
    Ui::Charts *ui;
/*!
 * \brief Obiekt serii danych
 */  
    QLineSeries *series;
/*!
 * \brief Obiekt wykresu
 */   
    QChart *chart;
/*!
 * \brief Wektor przechowujący wartości dla wykresu natężenia światła
 */ 
    QVector<double> qv_x, qv_y;
/*!
 * \brief Wektor przechowujący wartości dla wykresu temperatury
 */     
    QVector<double> qvtemp_x, qvtemp_y;
};

#endif // CHARTS_H
